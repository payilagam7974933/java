public class TamilNadu  extends Central_Govt
{
public static void main(String[] args)
{
Central_Govt cg=new TamilNadu();
//cg.give2000Rs();
cg.rice20kg();
cg.independent();							//Dynamic binding and
final int no=15;							// final  keyword:
// int=10;									//method,class,variable.
System.out.println(no);						// If we give final to method we cannot overridde the method.
}											// If we give final to class we cannot inheritate the classses.
public void give2000Rs()					//for variables we cannot re-assign the values for the variables.
{
System.out.println("2000");
}
public void independent()
{
super.independent();
System.out.println("TN-Independent");
}
}
//o/p
//rice
//CG-Independent
//TN-Independent
//15
