class Demo //method overloading
{
	public void show(int x)
	{
        System.out.println("int=" + x);
	}
	public void show(String s)
	{
        System.out.println("String=" + s);
    }
	public void show(byte b)
	{
        System.out.println("byte=" + b);
    }
	  public static void main(String[] args)
		  {
        byte a=2;
        Demo obj = new Demo();
		  
        obj.show(a);
  
        obj.show("hello");
  
        obj.show(250);
 
        obj.show('A');//ascii values
 
        obj.show("A");//ascii values
 
   
    }
}
