				//CONSTRUCTOR
//Java Program to demonstrate the use of the parameterized constructor.  
class Student3{  
    int id;  
    String name;  
    //creating a parameterized constructor  
    Student3(int i,String n){  
    id = i;  
    name = n;  
    }  
    //method to display the values  
    void display(){System.out.println(id+" "+name);}  
   
    public static void main(String args[]){  
    //creating objects and passing values  
    Student3 s1 = new Student3(111,"Karan");  
    Student3 s2 = new Student3(222,"Aryan");  
    //calling method to display the values of object  
    s1.display();  
    s2.display();  
   }  
}  